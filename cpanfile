requires 'Capture::Tiny',     '== 0.48';
requires 'File::Which',       '== 1.23';
requires 'File::chdir',       '== 0.1010';
requires 'Graph',             '== 0.96';
requires 'HTTP::Tiny',        '== 0.076';
requires 'Path::Tiny',        '== 0.108';
requires 'Text::FormatTable', '== 1.03';
requires 'YAML::PP',          '== 0.016';
requires 'Getopt::Long',      '== 2.50';
requires 'Pod::Usage',        '== 1.69';

on 'develop' => sub {
  requires 'App::FatPacker';
};
